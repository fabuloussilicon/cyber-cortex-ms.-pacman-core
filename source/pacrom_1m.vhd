library ieee;
  use ieee.std_logic_1164.all;
  use ieee.std_logic_unsigned.all;
  use ieee.numeric_std.all;

library UNISIM;
  use UNISIM.Vcomponents.all;

entity PACROM_1M is
  port (
    CLK         : in    std_logic;
    ENA         : in    std_logic;
    ADDR        : in    std_logic_vector(7 downto 0);
    DATA        : out   std_logic_vector(7 downto 0)
    );
end;

architecture rtl of PACROM_1M is
  type rom_array is array(0 to 255) of unsigned(7 downto 0);
  constant ROM : rom_array := (
        X"07", X"09", X"0a", X"0b", X"0c", X"0d", X"0d", X"0e",
		  X"0e", X"0e", X"0d", X"0d", X"0c", X"0b", X"0a", X"09", 
		  X"07", X"05", X"04", X"03", X"02", X"01", X"01", X"20",
		  X"20", X"20", X"01", X"01", X"02", X"03", X"04", X"05", 
		  X"07", X"0c", X"0e", X"0e", X"0d", X"0b", X"09", X"0a", 
		  X"0b", X"0b", X"0a", X"09", X"06", X"04", X"03", X"05",
		  X"07", X"09", X"0b", X"0a", X"08", X"05", X"04", X"03", 
		  X"03", X"04", X"05", X"03", X"01", X"20", X"20", X"02", 
		  X"07", X"0a", X"0c", X"0d", X"0e", X"0d", X"0c", X"0a", 
		  X"07", X"04", X"02", X"01", X"20", X"01", X"02", X"04",
		  X"07", X"0b", X"0d", X"0e", X"0d", X"0b", X"07", X"03", 
		  X"01", X"20", X"01", X"03", X"07", X"0e", X"07", X"20", 
		  X"07", X"0d", X"0b", X"08", X"0b", X"0d", X"09", X"06", 
		  X"0b", X"0e", X"0c", X"07", X"09", X"0a", X"06", X"02", 
		  X"07", X"0c", X"08", X"04", X"05", X"07", X"02", X"20", 
		  X"03", X"08", X"05", X"01", X"03", X"06", X"03", X"01", 
		  X"20", X"08", X"0f", X"07", X"01", X"08", X"0e", X"07",
		  X"02", X"08", X"0d", X"07", X"03", X"08", X"0c", X"07", 
		  X"04", X"08", X"0b", X"07", X"05", X"08", X"0a", X"07", 
		  X"06", X"08", X"09", X"07", X"07", X"08", X"08", X"07", 
		  X"07", X"08", X"06", X"09", X"05", X"0a", X"04", X"0b", 
		  X"03", X"0c", X"02", X"0d", X"01", X"0e", X"20", X"0f", 
		  X"20", X"0f", X"01", X"0e", X"02", X"0d", X"03", X"0c", 
		  X"04", X"0b", X"05", X"0a", X"06", X"09", X"07", X"08", 
		  X"20", X"01", X"02", X"03", X"04", X"05", X"06", X"07", 
		  X"08", X"09", X"0a", X"0b", X"0c", X"0d", X"0e", X"0f", 
		  X"0f", X"0e", X"0d", X"0c", X"0b", X"0a", X"09", X"08", 
		  X"07", X"06", X"05", X"04", X"03", X"02", X"01", X"20", 
		  X"20", X"01", X"02", X"03", X"04", X"05", X"06", X"07", 
		  X"08", X"09", X"0a", X"0b", X"0c", X"0d", X"0e", X"0f", 
		  X"20", X"01", X"02", X"03", X"04", X"05", X"06", X"07", 
		  X"08", X"09", X"0a", X"0b", X"0c", X"0d", X"0e", X"0f");  

begin

process (clk)
  begin
    if rising_edge(clk) then
      DATA <= std_logic_vector(ROM(to_integer(unsigned(ADDR))));
    end if;
  end process;

end rtl;




